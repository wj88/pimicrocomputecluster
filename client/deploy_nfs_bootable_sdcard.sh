#!/bin/bash

# Created by 2015 Dr Jodie Wetherall <wj88@gre.ac.uk>
# Licensed under terms of GPLv3

SCRIPTPATH=$(basename $0)
LOGFILE=/var/log/$0.log
PACKAGES="parted nfs-common util-linux"

IPPREFIX="192.168.137.1" # This will have the node index left padding with zeros and appended to the end
GATEWAY="192.168.137.1"
NETMASK="255.255.255.0"
DNSSERVER="8.8.8.8"

usage()
{
	echo -e "\nUsage:"
	echo -e "sudo $SCRIPTPATH ..."
	echo -e "-d {Device, eg /dev/sdd}"
	echo -e "-i {Image Name, eg pi-image}"
	echo -e "-n {NFS Server Address, eg 192.168.0.1}"
	echo -e "-u {Node Index, 1..99}"
	echo -e "-m {Netmask, default is 255.255.255.0}"
	echo -e "-g {Gateway, default is 192.168.137.1}"
	echo -e "-l {DNS Server, default is 8.8.8.8}"
	echo -e "-a {Target IP Prefix, eg 192.168.0.1XX}"
	echo -e "\tXX is appended based on the node index."
	echo -e "\tDefault value is: 192.168.137.1"
	echo -e ""
	echo -e "\nFor example:"
	echo -e "sudo $SCRIPTPATH -d /dev/sdc -i pi-image -n 192.168.0.1 -u 1\n"
}

checkpackages()
{
        # Check through our package list to see if any
        # need installing.
        NEEDSINSTALL=0
        for PACKAGE in $PACKAGES; do
                dpkg -s $PACKAGE >/dev/null 2>/dev/null
                if [ $? -ne 0 ]; then
                        NEEDSINSTALL=1
                fi
        done
        # If Any require installation, start the install process.
        if [ $NEEDSINSTALL -ne 0 ]; then
                echo -e "Installing prerequisite packages..." | tee -a $LOGFILE
                apt-get update >>$LOGFILE 2>>$LOGFILE
                apt-get -y install $PACKAGES >>$LOGFILE 2>>$LOGFILE
        fi
}

tidydir()
{
        if [ "$1" != "" ]; then
                if [ -d $1 ]; then
                        rm -Rf $1 >>$LOGFILE 2>>$LOGFILE
                fi
        fi
}

tidymountpoint()
{
        if [ "$1" != "" ]; then
                if [ $(mount | grep " on $1 " | wc -l) -ne 0 ]; then
                        umount $1 >>$LOGFILE 2>>$LOGFILE
                fi
                tidydir $1
        fi
}

tidyup()
{
        tidymountpoint $MOUNTPOINT
}

# We must be root to run!
if [[ $EUID -ne 0 ]]; then
	echo -e "You must run this script as root."
	exit 1
fi

echo -e "\nRaspbery Pi NFS Hosted SD Card Creation Script\n==============================================\n\nWritten by Dr Jodie Wetherall <wj88@gre.ac.uk>"

while getopts ":d:i:n:u:a:m:g:l:h" OPT; do
        case $OPT in
                d)
                        DRIVE=$OPTARG
                ;;
                i)
                        NAME=$OPTARG
                ;;
                n)
                        NFS=$OPTARG
                ;;
                u)
                        NODEINDEX=$OPTARG
                ;;
		a)
			IPPREFIX=$OPTARG
		;;
		m)
			NETMASK=$OPTARG
		;;
		g)
			GATEWAY=$OPTARG
		;;
		l)
			DNSSERVER=$OPTARG
		;;
                h)
                        usage
                        exit 0
                ;;
                \?)
                        echo -e "Invalid option: -$OPTARG"
                        usage
                        exit 1
                ;;
                :)
                        echo -e "Option -$OPTARG requires an argument."
                        usage
                        exit 1
                ;;
        esac
done;

# Check the device
if [ ! -b $DRIVE ]; then
	echo -e "No block device specified."
	usage
	exit 1
fi

# Check the name of the image to deploy
if [ "$NAME" = "" ]; then
	echo -e "No image name specified."
	usage
	exit 1
fi

# Check the NFS server address
if [ "$NFS" = "" ]; then
	echo -e "No NFS server address provided."
	usage
	exit 1
fi

# Check the node index - must be numberic
if ! [ "$NODEINDEX" -eq "$NODEINDEX" ] 2>>/dev/null; then
	echo -e "A valid node index was not provided."
	usage
	exit 1
fi

# Now let's setup our logfile
echo -e "server_boot_image_restore: Starting at $(date)" >>$LOGFILE

# Do we need to install some software?
checkpackages

IP="${IPPREFIX}$(printf %02d ${NODEINDEX})"

echo -e "Device: ${DRIVE}" | tee -a $LOGFILE
echo -e "Image Name: ${NAME}" | tee -a $LOGFILE
echo -e "NFS Server: ${NFS}" | tee -a $LOGFILE
echo -e "Node Index: ${NODEINDEX}" | tee -a $LOGFILE
echo -e "IP: ${IP}" | tee -a $LOGFILE
echo -e "Gateway: ${GATEWAY}" | tee -a $LOGFILE
echo -e "Netmask : ${NETMASK}" | tee -a $LOGFILE
echo -e "DNS: ${DNSSERVER}" | tee -a $LOGFILE

# Unmount if necessary...
umount ${DRIVE}* >>$LOGFILE 2>>$LOGFILE

# Now create the mount point mount point
MOUNTPOINT=/mnt/NFSMntDeploy
TARGETPREFIX=${MOUNTPOINT}/${NAME}_
if [ ! -d $MOUNTPOINT ]; then
	mkdir $MOUNTPOINT >>$LOGFILE 2>>$LOGFILE
fi

# Mount the NFS share
echo -e "Mounting NFS Share.." | tee -a $LOGFILE
mount $NFS:/exports/piboot $MOUNTPOINT >>$LOGFILE 2>>$LOGFILE

# Did the mount succeed?
if [ $? -ne 0 ]; then
	echo -e "\tFailed to mount the NFS share. Return code $?." | tee -a $LOGFILE
	exit 2
fi

# Does the partition file exist?
if [ ! -e ${TARGETPREFIX}parttable ]; then
	echo -e "\tThe partition table file, ${NAME}_parttable, does not exist on the NFS share." | tee -a $LOGFILE
	tidyup
	exit 3
fi

# Does the partition file exist?
if [ ! -e ${TARGETPREFIX}boot.img ]; then
	echo -e "\tThe boot partition image file, ${NAME}boot.img, does not exist on the NFS share." | tee -a $LOGFILE
	tidyup
	exit 4
fi

# Repartitioning
echo -e "Repartitioning the SD card..." | tee -a $LOGFILE
#cat ${TARGETPREFIX}parttable | sfdisk ${DRIVE} >>$LOGFILE 2>>$LOGFILE
BLOCKSIZE=$(cat ${TARGETPREFIX}parttable 2>>/dev/null | grep "blocks of" | cut -d" " -f 5)
VFATSTARTBLOCKS=$(cat ${TARGETPREFIX}parttable 2>>/dev/null | grep "FAT32" | tr -s ' ' | cut -d" " -f 2)
VFATCOUNTBLOCKS=$(cat ${TARGETPREFIX}parttable 2>>/dev/null | grep "FAT32" | tr -s ' ' | cut -d" " -f 4)
EXTSTARTBLOCKS=$(cat ${TARGETPREFIX}parttable 2>>/dev/null | grep "Linux" | tr -s ' ' | cut -d" " -f 2)
{
echo -e $VFATSTARTBLOCKS,$VFATCOUNTBLOCKS,0x0C,*
echo -e $EXTSTARTBLOCKS,,,-
} | sfdisk -q -L -D -u B $DRIVE >>$LOGFILE 2>>$LOGFILE
partprobe ${DRIVE} >>$LOGFILE 2>>$LOGFILE

# We need to check to see whether the device name has a "p" in the
# name and then reference it accordingly.
VFATPATH=''
if [ -b ${DRIVE}1 ]; then
	VFATPATH=${DRIVE}1
else
	if [ -b ${DRIVE}p1 ]; then
		VFATPATH=${DRIVE}p1
	else
		echo -e "\tCannot find fat32 partition in /dev." | tee -a $LOGFILE
		tidyup
		exit 5
	fi
fi

# The same thing now but this time with the main linux partition.
EXTPATH=''
if [ -b ${DRIVE}2 ]; then
	EXTPATH=${DRIVE}2
else
	if [ -b ${DRIVE}p2 ]; then
		EXTPATH=${DRIVE}p2
	else
		echo -e "\tCannot find the ext partition in /dev." | tee -a $LOGFILE
		tidyup
		exit 5
	fi
fi

# Now mount the fat32 partition
if [ ! "$VFATPATH" = "" ]; then
	# Write the boot image to the first partition of the SD card from the NFS share.
	echo -e "Deploying the boot partition..." | tee -a $LOGFILE
	dd if=${TARGETPREFIX}boot.img of=${VFATPATH} bs=4M >>$LOGFILE 2>>$LOGFILE
	if [ $? -ne 0 ]; then
		echo -e "\tFailed to deploy the boot partition." | tee -a $LOGFILE
		tidyup
		exit 6
	fi

	# Unmount the NFS share
	umount $MOUNTPOINT >>$LOGFILE 2>>$LOGFILE

	# Mount the new boot partition to update it's contents
	echo -e "Updating the boot filesystem contents..." | tee -a $LOGFILE
	mount -t vfat $VFATPATH $MOUNTPOINT >>$LOGFILE 2>>$LOGFILE
	if [ $? -ne 0 ]; then
		echo -e "\tFailed to mount the boot partition after successfully deploying it." | tee -a $LOGFILE
		tidyup
		exit 7
	fi

	# Set the IP address and hostname in the kernel arguments
	echo -e "dwc_otg.lpm_enable=0 console=ttyAMA0,115200 console=tty1 elevator=deadline rootwait root=/dev/nfs rootfstype=nfs nfsroot=${NFS}:/exports/piboot/${NAME}_rootfs,tcp,vers=3 nfs-options=hard,intr,ro ip=${IP}:${NFS}:${GATEWAY}:${NETMASK}:${NAME}-${NODEINDEX}:eth0 dns=${DNSSERVER}" >$MOUNTPOINT/cmdline.txt

	# This is to configure 1080p HDMI output mode. This was needed
	# for my project although maybe not for everyone!
	sed -i s/#hdmi_group=1/hdmi_group=2/g $MOUNTPOINT/config.txt
	sed -i s/#hdmi_mode=1/hdmi_mode=82/g $MOUNTPOINT/config.txt
	sed -i s/#hdmi_drive=2/hdmi_drive=2/g $MOUNTPOINT/config.txt

	sync >>$LOGFILE 2>>$LOGFILE
fi

if [ ! "$EXTPATH" = "" ]; then
	echo -e "Formatting the root filesystem..." | tee -a $LOGFILE
	mkfs.ext4 -L rootfs ${EXTPATH} >>$LOGFILE 2>>$LOGFILE
fi

#Tidy up our mount point
sync >>$LOGFILE 2>>$LOGFILE
tidyup

echo -e "Done." | tee -a $LOGFILE

echo -e "\nThe SD card is now deployed. You can safely remove the card and insert this\ninto your Raspberry Pi.\n"
