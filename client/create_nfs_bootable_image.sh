#! /bin/bash

# Created by 2015 Dr Jodie Wetherall <wj88@gre.ac.uk>
# Licensed under terms of GPLv3

SCRIPTPATH=$(basename $0)
LOGFILE=/var/log/$0.log
PACKAGES="util-linux parted rsync nfs-common curl openssh-client"
INCLUDECLUSTERTOOLS=0
IPPREFIX=""

usage()
{
	echo -e "\nUsage:"
	echo -e "sudo ${SCRIPTPATH} ..."
	echo -e "-c (SD | IMG | URL)"
	echo -e "\t-c SD -d {Device, eg /dev/sdd}"
	echo -e "\t-c IMG -s {Image Path, eg /tmp/source-pi.img)"
	echo -e "\t-c URL -u {URL, eg https://downloads.raspberrypi.org/raspbian_latest}"
	echo -e "-i {Image Name, eg pi-image}"
	echo -e "-n {NFS Server Address, eg 192.168.0.1}"
	echo -e "-C {IP Prefix, include cluster tools}"
	echo -e "\nTo create an image from an SD card:"
	echo -e "sudo $SCRIPTPATH -c SD -d /dev/sdd -i pi-image -n 192.168.0.1"
	echo -e "\nTo create an image from a local image file:"
	echo -e "sudo $SCRIPTPATH -c IMG -s /tmp/source-pi.img -i pi-image -n 192.168.0.1"
	echo -e "\nTo create an image from an online image file:"
	echo -e "sudo $SCRIPTPATH -c URL -u https://downloads.raspberrypi.org/raspbian_latest -i pi-image -n 192.168.0.1\n"
}

checkpackages()
{
	# Check through our package list to see if any
	# need installing.
	NEEDSINSTALL=0
	for PACKAGE in $PACKAGES; do
		dpkg -s $PACKAGE >/dev/null 2>/dev/null
		if [ $? -ne 0 ]; then
			NEEDSINSTALL=1
		fi
	done
	# If Any require installation, start the install process.
	if [ $NEEDSINSTALL -ne 0 ]; then
		echo -e "Installing prerequisite packages..." | tee -a $LOGFILE
		apt-get update >>$LOGFILE 2>>$LOGFILE
		apt-get -y install $PACKAGES >>$LOGFILE 2>>$LOGFILE
	fi
}

tidydir()
{
	if [ "$1" != "" ]; then
		if [ -d $1 ]; then
			rm -Rf $1 >>$LOGFILE 2>>$LOGFILE
		fi
	fi
}

tidymountpoint()
{
	if [ "$1" != "" ]; then
		if [ $(mount | grep " on $1 " | wc -l) -ne 0 ]; then
			umount $1 >>$LOGFILE 2>>$LOGFILE
		fi
		tidydir $1
	fi

}

tidyup()
{
	tidymountpoint $MOUNTPOINT
	tidymountpoint $EXTMOUNTPOINT
	tidydir $TMPDWNDIR
	tidydir $TMPUNCMPDIR
}

# We must be root to run!
if [[ $EUID -ne 0 ]]; then
	echo -e "You must run this script as root."
	exit 1
fi

echo -e "\nRaspbery Pi NFS Hosted Root Filesystem Creation Script\n======================================================\n\nWritten by Dr Jodie Wetherall <wj88@gre.ac.uk>"

# Read the arguments from the command line
while getopts ":c:d:u:s:i:n:C:h" OPT; do
	case $OPT in
		c)
			CMDTYPE=$OPTARG
			if [ "$CMDTYPE" != "SD" -a "$CMDTYPE" != "IMG" -a "$CMDTYPE" != "URL"  ]; then
				echo -e "You must specify the type of action (SD or IMG - $CMDTYPE)."
				usage
			fi
		;;
		d)
			DRIVE=$OPTARG
		;;
		u)
			URL=$OPTARG
		;;
		s)
			IMGFILE=$OPTARG
		;;
		i)
			NAME=$OPTARG
		;;
		n)
			NFS=$OPTARG
		;;
		C)
			INCLUDECLUSTERTOOLS=1
			IPPREFIX=$OPTARG
		;;
		h)
			usage
			exit 0
		;;
		\?)
			echo -e "Invalid option: -$OPTARG"
			usage
			exit 1
		;;
		:)
			echo -e "Option -$OPTARG requires an argument.".
			usage
			exit 1
		;;
	esac
done;


if [ "$CMDTYPE" = "SD" ]; then
	if [ ! -b $DRIVE ]; then
		echo -e "No block device specified."
		usage
		exit 1
	fi
fi

if [ "$CMDTYPE" = "IMG" ]; then
	# Grab the image filename from the command line argument
	if [ ! -e $IMGFILE ]; then
		echo -e "No valid image file specified."
		usage
		exit 1
	fi
fi

if [ "$CMDTYPE" = "URL" ]; then
	if [ "$URL" = "" ]; then
		echo -e "No URL specified."
		usage
		exit 1
	fi
fi

if [ "$NAME" = "" ]; then
	echo -e "No image name specified."
	usage
	exit 1
fi

# Grab the NFS server info
if [ "$NFS" = "" ]; then
	echo -e "No NFS server information provided."
	usage
	exit 1
fi

# Now let's setup our logfile
echo -e "server_boot_image_create: Starting at $(date)" >>$LOGFILE
TMPDWNDIR=""
TMPUNCMPDIR=""

# Install any prerequisite packages
checkpackages

# If this is a device
if [ "$CMDTYPE" = "SD" ]; then
	echo -e "Device: ${DRIVE}" | tee -a $LOGFILE

	# Unmount if necessary...
	umount ${DRIVE}* >>$LOGFILE 2>>$LOGFILE
fi

echo -e "Target Image Name: ${NAME}" | tee -a $LOGFILE
echo -e "NFS Server: ${NFS}" | tee -a $LOGFILE

# If this is a URL, download it.
if [ "$CMDTYPE" = "URL" ]; then

	echo "Source URL: $URL" | tee -a $LOGFILE

	echo -e "Attempting to download the image..." | tee -a $LOGFILE
	TMPDWNDIR=$(mktemp -d)
	(cd $TMPDWNDIR; curl -L -O $URL)
	IMGFILE=$TMPDWNDIR/$(ls $TMPDWNDIR)
	if [ "$IMGFILE" = "$TMPDWNDIR/" ]; then
		echo -e "\tNo image file downloaded."
		tidyup
		exit 2
	fi
	CMDTYPE="IMG"
fi

# If we're working with an img file, it may be compressed.
if [ "$CMDTYPE" = "IMG" ]; then

	# If this is a zip file, extract it.
	if file $IMGFILE | grep -q "Zip archive"; then
		echo -e "Attempting to uncompress the image from $IMG..." | tee -a $LOGFILE
		TMPUNCMPDIR=$(mktemp -d)
		(cd $TMPUNCMPDIR; unzip $IMGFILE | tee -a $LOGFILE)
		IMGFILE=$TMPUNCMPDIR/$(ls $TMPUNCMPDIR)
		if [ "$IMGFILE" = "$TMPUNCMPDIR/" ]; then
			echo -e "\tNo image file extracted."
			tidyup
			exit 2
		fi
		if [ -a /tmp/$(basename $IMGFILE) ]; then
			rm /tmp/$(basename $IMGFILE)
		fi
		mv $IMGFILE /tmp >>$LOGFILE 2>>$LOGFILE
		IMGFILE=/tmp/$(basename $IMGFILE)
	fi

	# Do a last check to ensure this is an image file
	if ! file $IMGFILE | grep -q "x86 boot sector"; then
		echo -e "We've not ended up with an image file.\n$(file $IMGFILE)" | tee -a $LOGFILE
		tidyup
		exit 1
	fi

	echo -e "Image File: $IMGFILE" | tee -a $LOGFILE

	DRIVE=$IMGFILE
fi

# Now create the mount point mount point
MOUNTPOINT=/mnt/NFSMntCreate
EXTMOUNTPOINT=${MOUNTPOINT}EXT
TARGETPREFIX=${MOUNTPOINT}/piboot/${NAME}_
if [ ! -d $MOUNTPOINT ]; then
	mkdir $MOUNTPOINT >>$LOGFILE 2>>$LOGFILE
fi
if [ ! -d $EXTMOUNTPOINT ]; then
	mkdir $EXTMOUNTPOINT >>$LOGFILE 2>>$LOGFILE
fi

# Mount the NFS share
echo -e "Mounting NFS Share..." | tee -a $LOGFILE
mount $NFS:/exports $MOUNTPOINT >>$LOGFILE 2>>$LOGFILE

# Did the mount succeed?
if [ $? -ne 0 ]; then
	echo -e "\tFailed to mount the NFS share. Return code $?." | tee -a $LOGFILE
	tidyup
	exit 2
fi

# Does the partition file exist?
if [ -a ${TARGETPREFIX}parttable ]; then
	rm ${TARGETPREFIX}parttable >>$LOGFILE 2>>$LOGFILE
fi

# Create the partition table file.
echo -e "Creating the partition table file..." | tee -a $LOGFILE
sfdisk -l -u B ${DRIVE} > ${TARGETPREFIX}parttable 2>>$LOGFILE

# Did the sfdisk succeed?
if [ $? -ne 0 ]; then
	echo -e "\tFailed to create the partition table file." | tee -a $LOGFILE
	tidyup
	exit 3
fi

# Does the partition file exist?
if [ -a ${TARGETPREFIX}boot.img ]; then
	rm ${TARGETPREFIX}boot.img >>$LOGFILE 2>>$LOGFILE
fi

if [ "$CMDTYPE" = "SD" ]; then

	# We need to check to see whether the device name has a "p" in the
	# name and then reference it accordingly.
	VFATPATH=''
	if [ -b ${DRIVE}1 ]; then
		VFATPATH=${DRIVE}1
	else
		if [ -b ${DRIVE}p1 ]; then
			VFATPATH=${DRIVE}p1
		else
			echo -e "\tCannot find fat32 partition in /dev." | tee -a $LOGFILE
			tidyup
			exit 4
		fi
	fi

	# The same thing now but this time with the main linux partition.
	EXTPATH=''
	if [ -b ${DRIVE}2 ]; then
		EXTPATH=${DRIVE}2
	else
		if [ -b ${DRIVE}p2 ]; then
			EXTPATH=${DRIVE}p2
		else
			echo -e "\tCannot find the ext partition in /dev." | tee -a $LOGFILE
			tidyup
			exit 4
		fi
	fi

	echo -e "Creating the boot partition image..." | tee -a $LOGFILE
	dd if=$VFATPATH of=${TARGETPREFIX}boot.img bs=4M
	if [ $? -ne 0 ]; then
		echo -e "\tFailed to backup the fat32 partition." | tee -a $LOGFILE
		tidyup
		exit 5
	fi

	echo -e "Mounting the root filesystem..." | tee -a $LOGFILE
	mount $EXTPATH $EXTMOUNTPOINT >>$LOGFILE 2>>$LOGFILE
	if [ $? -ne 0 ]; then
		echo -e "\tFailed to mount the ext partition." | tee -a $LOGFILE
		tidyup
		exit 6
	fi

	# Clean up bits we don't need anymore
	rm -Rf $EXTMOUNTPOINT/tmp/* >>$LOGFILE 2>>$LOGFILE
	rm -Rf $EXTMOUNTPOINT/var/log/*.gz >>$LOGFILE 2>>$LOGFILE
	find $EXTMOUNTPOINT/var/log/ -type f -exec truncate -c -s 0 {} \; >>$LOGFILE 2>>$LOGFILE
	find $EXTMOUNTPOINT/var/cache/apt/ -type f -exec rm {} \; >>$LOGFILE 2>>$LOGFILE
	truncate -c -s 0 $EXTMOUNTPOINT/root/.bash_history >>$LOGFILE 2>>$LOGFILE
fi


if [ "$CMDTYPE" = "IMG" ]; then

	echo -e "Creating the boot partition image..." | tee -a $LOGFILE

	BLOCKSIZE=$(sfdisk -l -u B $IMGFILE 2>>/dev/null | grep "blocks of" | cut -d" " -f 5)
	EXTSTARTBLOCKS=$(sfdisk -l -u B $IMGFILE 2>>/dev/null | grep "Linux" | tr -s ' ' | cut -d" " -f 2)
	VFATSTARTBLOCKS=$(sfdisk -l -u B $IMGFILE 2>>/dev/null | grep "FAT32" | tr -s ' ' | cut -d" " -f 2)
	VFATCOUNTBLOCKS=$(sfdisk -l -u B $IMGFILE 2>>/dev/null | grep "FAT32" | tr -s ' ' | cut -d" " -f 4)
	EXTSTARTBYTES=`echo -e $EXTSTARTBLOCKS*$BLOCKSIZE | bc`

	dd if=$IMGFILE of=${TARGETPREFIX}boot.img count=$VFATCOUNTBLOCKS bs=$BLOCKSIZE skip=$VFATSTARTBLOCKS >>$LOGFILE 2>>$LOGFILE
	if [ $? -ne 0 ]; then
		echo -e "\tFailed to backup the fat32 partition." | tee -a $LOGFILE
		tidyup
		exit 5
	fi

	echo -e "Mounting the root filesystem..." | tee -a $LOGFILE
	mount -o ro,loop,offset=$EXTSTARTBYTES $IMGFILE $EXTMOUNTPOINT >>$LOGFILE 2>>$LOGFILE
	if [ $? -ne 0 ]; then
		echo -e "\tFailed to mount the ext partition." | tee -a $LOGFILE
		tidyup
		exit 6
	fi

fi


echo -e "Creating an NFS clone of the root filesystem..." | tee -a $LOGFILE

# Backup the rootfs
if [ ! -d ${TARGETPREFIX}rootfs ]; then
	mkdir ${TARGETPREFIX}rootfs >>$LOGFILE 2>>$LOGFILE
fi
rsync -WaxHAX --delete-before --numeric-ids --info=progress2 ${EXTMOUNTPOINT}/ ${TARGETPREFIX}rootfs

# Needs a little work on the NFS rootfs to make it NFS bootable.
echo -e "Configuring the NFS cloned root filesystem for SSH..." | tee -a $LOGFILE

# Get rid of the /etc/hostname file to force the boot process to use
# the kernel argument version
rm ${TARGETPREFIX}rootfs/etc/hostname >>$LOGFILE 2>>$LOGFILE

# Create the Pi users ssh configuration
mkdir -p ${TARGETPREFIX}rootfs/home/pi/.ssh >>$LOGFILE 2>>$LOGFILE

# Generate the SSH key
ssh-keygen -t rsa -q -f ${TARGETPREFIX}rootfs/home/pi/.ssh/id_rsa -N "" -C "" >>$LOGFILE 2>>$LOGFILE

# Allow a Pi with a matching SSH key to auto-authenticate with us. Basically,
# other Pis booting from the same NFS share can connect to sibling Pis.
cat ${TARGETPREFIX}rootfs/home/pi/.ssh/id_rsa.pub >${TARGETPREFIX}rootfs/home/pi/.ssh/authorized_keys2 2>>$LOGFILE

# Specific to a cluster who would never has spoken to other cluster
# nodes before, disable the alert about keys which have never been seen before.
echo -e "Host *" >${TARGETPREFIX}rootfs/home/pi/.ssh/config 2>>$LOGFILE
echo -e "\tStrictHostKeyChecking no" >>${TARGETPREFIX}rootfs/home/pi/.ssh/config 2>>$LOGFILE

# Configure permissions of all the ssh files.
chown -R 1000:1000 ${TARGETPREFIX}rootfs/home/pi/.ssh >>$LOGFILE 2>>$LOGFILE
chmod 700 ${TARGETPREFIX}rootfs/home/pi/.ssh >>$LOGFILE 2>>$LOGFILE

# Install the unionfs-fuse package into the new rootfs
echo -e "Installing UnionFS Fuse and enabling SSH Server..." | tee -a $LOGFILE
./modify_nfs_bootable_image.sh -i ${NAME} -n ${NFS} -c "apt-get update; apt-get -y install unionfs-fuse; dpkg-reconfigure openssh-server" >>$LOGFILE 2>>$LOGFILE

# Hijack the boot process to allow the node to use unionfs
echo -e "Configuring UnionFS into the NFS cloned root filesystem..." | tee -a $LOGFILE 2>>$LOGFILE
mkdir ${TARGETPREFIX}rootfs/mnt/sd_rootfs  >>$LOGFILE 2>>$LOGFILE
mkdir ${TARGETPREFIX}rootfs/mnt/union_rootfs >>$LOGFILE 2>>$LOGFILE
mv ${TARGETPREFIX}rootfs/sbin/init ${TARGETPREFIX}rootfs/sbin/init2 >>$LOGFILE 2>>$LOGFILE
cat <<EOF >${TARGETPREFIX}rootfs/sbin/init
#!/bin/sh
echo Switching to rewritable union-fs...
modprobe fuse
mount /dev/mmcblk0p2 /mnt/sd_rootfs
rm -Rf /mnt/sd_rootfs/tmp/*
/usr/bin/unionfs-fuse -o cow,allow_other,suid,dev,nonempty,max_files=32768 /mnt/sd_rootfs=RW:/=RO /mnt/union_rootfs
exec switch_root /mnt/union_rootfs /sbin/init2
EOF
chmod +x ${TARGETPREFIX}rootfs/sbin/init >>$LOGFILE 2>>$LOGFILE

# Include cluster tools if we want them
#if [ "$INCLUDECLUSTERTOOLS" = "1" ]; the
	cp ../cluster/* ${TARGETPREFIX}rootfs/usr/local/bin
	sed -i ${TARGETPREFIX}rootfs/usr/local/bin/cluster-find 's/IPPREFIX=193.168.137.1/IPPREFIX=${IPPREFIX}'
	sed -i ${TARGETPREFIX}rootfs/usr/local/bin/cluster-missing 's/IPPREFIX=193.168.137.1/IPPREFIX=${IPPREFIX}'
fi

#Tidy up our mount point
echo -e "Tidying up..."
sync >>$LOGFILE 2>>$LOGFILE
tidyup

echo -e "Done."

echo -e "\nYou can now generate an SD card using the following:\nsudo deploy_nfs_bootable_sdcard.sh -d {Device, eg /dev/sdd} -i ${NAME} -n ${NFS} -u {Node Index, eg 1..n}\n\nOr you can modify the NFS hosted root filesystem using the following:\nsudo modify_nfs_bootable_image.sh -i ${NAME} -n ${NFS}\n" | tee -a $LOGFILE
