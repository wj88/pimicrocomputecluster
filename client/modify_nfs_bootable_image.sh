#!/bin/bash

# Created by 2015 Jodie Wetherall <wj88@gre.ac.uk>
# Licensed under terms of GPLv3

SCRIPTPATH=$(basename $0)
LOGFILE=/var/log/$0.log
PACKAGES="nfs-common qemu-user-static"

usage()
{
	echo -e "\nUsage:"
	echo -e "sudo ${SCRIPTPATH} ..."
	echo -e "-i {Image Name, eg pi-image}"
	echo -e "-n {NFS Server Address, eg 192.168.0.1}"
	echo -e "-c {Command  and arguments, eg \"apt-get update\"}"
	echo -e "\nTo access a Pi root filesystem command line:"
	echo -e "sudo $SCRIPTPATH -i pi-image -n 192.168.0.1"
	echo -e "\nTo execute a single command in the root filesystem:"
	echo -e "sudo $SCRIPTPATH -i pi-image -n 192.168.0.1 -c \"apt-get update\""
	echo -e "\nTo execute a set of commands in the root filesystem:"
	echo -e "sudo $SCRIPTPATH -i pi-image -n 192.168.0.1 -c \"apt-get update; apt-get install curl\"\n"
	exit 1
}

checkpackages()
{
        # Check through our package list to see if any
        # need installing.
        NEEDSINSTALL=0
        for PACKAGE in $PACKAGES; do
                dpkg -s $PACKAGE >/dev/null 2>/dev/null
                if [ $? -ne 0 ]; then
                        NEEDSINSTALL=1
                fi
        done
        # If Any require installation, start the install process.
        if [ $NEEDSINSTALL -ne 0 ]; then
                echo -e "Installing prerequisite packages..." | tee -a $LOGFILE
                apt-get update >>$LOGFILE 2>>$LOGFILE
                apt-get -y install $PACKAGES >>$LOGFILE 2>>$LOGFILE
        fi
}

# We must be root to run!
if [[ $EUID -ne 0 ]]; then
        echo -e "You must run this script as root, eg:\n\tsudo $SCRIPTPATH" >&2
        exit 1
fi

echo -e "\nRaspbery Pi NFS Hosted Root Filesystem Modification Script\n==========================================================\n\nWritten by Dr Jodie Wetherall <wj88@gre.ac.uk>" >&2

# Read the arguments from the command line
while getopts ":c:d:u:s:i:n:h" OPT; do
        case $OPT in
                i)
                        NAME=$OPTARG
                ;;
                n)
                        NFS=$OPTARG
                ;;
		c)
			CMDARGS="$OPTARG"
		;;
                h)
                        usage
                        exit 0
                ;;
                \?)
                        echo -e "Invalid option: -$OPTARG" >&2
                        usage
                        exit 1
                ;;
                :)
                        echo -e "Option -$OPTARG requires an argument.". >&2
                        usage
                        exit 1
		;;
        esac
done;


# Grab the image name from the command line argument
if [ "$NAME" = "" ]; then
	echo -e "No image name specified." | >&2
	usage
fi

# Grab the NFS server information from the command line argument
if [ "$NFS" = "" ]; then
	echo -e "No NFS server information provided." | >&2
	usage
fi

# Now let's setup our logfile
echo -e "server_boot_image_modify: Starting at $(date)" >>$LOGFILE

# Do we need to install some software?
checkpackages

# Now create the mount point mount point
MOUNTPOINT=/mnt/NFSMntModify
TARGETPREFIX=$MOUNTPOINT/piboot/${NAME}_
if [ ! -d $MOUNTPOINT ]; then
        mkdir $MOUNTPOINT >>$LOGFILE 2>>$LOGFILE
fi

# Mount the NFS share
echo -e "Mounting NFS Share..." | tee -a $LOGFILE >&2
mount $NFS:/exports $MOUNTPOINT >>$LOGFILE 2>>$LOGFILE

# Did the mount succeed?
if [ $? -ne 0 ]; then
        echo -e "\tFailed to mount the NFS share. Return code $?." | tee -a $LOGFILE >&2
        rmdir $MOUNTPOINT >>$LOGFILE 2>>$LOGFILE
        exit 2
fi

BOOT_IMG=${TARGETPREFIX}boot.img
ROOTFS_PATH=${TARGETPREFIX}rootfs

echo -e "Setting up the root filesystem for chroot..." | tee -a $LOGFILE >&2
cp /usr/bin/qemu-arm-static ${ROOTFS_PATH}/usr/bin >>$LOGFILE 2>>$LOGFILE
mount -o loop -t vfat ${BOOT_IMG} ${ROOTFS_PATH}/boot >>$LOGFILE 2>>$LOGFILE
mount -o bind /dev ${ROOTFS_PATH}/dev >>$LOGFILE 2>>$LOGFILE
mount -o bind /proc ${ROOTFS_PATH}/proc >>$LOGFILE 2>>$LOGFILE
mount -o bind /sys ${ROOTFS_PATH}/sys >>$LOGFILE 2>>$LOGFILE
mount -o bind /dev/pts ${ROOTFS_PATH}/dev/pts >>$LOGFILE 2>>$LOGFILE
mv ${ROOTFS_PATH}/etc/network/interfaces ${ROOTFS_PATH}/etc/network/interfaces.bak >>$LOGFILE 2>>$LOGFILE
mv ${ROOTFS_PATH}/etc/resolv.conf ${ROOTFS_PATH}/etc/resolv.conf.bak >>$LOGFILE 2>>$LOGFILE
cp /etc/network/interfaces ${ROOTFS_PATH}/etc/network/ >>$LOGFILE 2>>$LOGFILE
cp /etc/resolv.conf ${ROOTFS_PATH}/etc/ >>$LOGFILE 2>>$LOGFILE
sed -i.bak -e 's/^/#/' ${ROOTFS_PATH}/etc/ld.so.preload >>$LOGFILE 2>>$LOGFILE

echo -e "Please note, to permit emulation of the root filesystem you are editing, the\nfollowing files cannot be edited while using this tool:\n\t/etc/network/interfaces\n\t/etc/resolv.conf\n\t/etd/ld.so.preload\nTo edit these files, edit the '.bak' versions" >&2

if [ "${CMDARGS}" = "" ]; then
	echo -e "Chrooting into the root fileystem, type exit when you're done..." | tee -a $LOGFILE >&2
	chroot ${ROOTFS_PATH} ${CMDARGS}
else
	echo -e "Running \"${CMDARGS}\" in the chrooted fileystem..." | tee -a $LOGFILE >&2
	chroot ${ROOTFS_PATH} /bin/bash -c "${CMDARGS}"
fi


echo -e "Cleaning up the root filesystem following a chroot..." | tee -a $LOGFILE >&2
rm ${ROOTFS_PATH}/usr/bin/qemu-arm-static >>$LOGFILE 2>>$LOGFILE
rm ${ROOTFS_PATH}/etc/network/interfaces >>$LOGFILE 2>>$LOGFILE
rm ${ROOTFS_PATH}/etc/resolv.conf >>$LOGFILE 2>>$LOGFILE
rm ${ROOTFS_PATH}/etc/ld.so.preload >>$LOGFILE 2>>$LOGFILE
mv ${ROOTFS_PATH}/etc/network/interfaces.bak ${ROOTFS_PATH}/etc/network/interfaces >>$LOGFILE 2>>$LOGFILE
mv ${ROOTFS_PATH}/etc/resolv.conf.bak ${ROOTFS_PATH}/etc/resolv.conf >>$LOGFILE 2>>$LOGFILE
mv ${ROOTFS_PATH}/etc/ld.so.preload.bak ${ROOTFS_PATH}/etc/ld.so.preload >>$LOGFILE 2>>$LOGFILE
rm -Rf ${ROOTFS_PATH}/tmp/* >>$LOGFILE 2>>$LOGFILE
umount ${ROOTFS_PATH}/dev/pts >>$LOGFILE 2>>$LOGFILE
umount ${ROOTFS_PATH}/boot >>$LOGFILE 2>>$LOGFILE
umount ${ROOTFS_PATH}/dev >>$LOGFILE 2>>$LOGFILE
umount ${ROOTFS_PATH}/proc >>$LOGFILE 2>>$LOGFILE
umount ${ROOTFS_PATH}/sys >>$LOGFILE 2>>$LOGFILE

umount $MOUNTPOINT >>$LOGFILE 2>>$LOGFILE
rmdir $MOUNTPOINT >>$LOGFILE 2>>$LOGFILE

echo -e "Done.\n" >&2
