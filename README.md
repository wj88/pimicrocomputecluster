# Raspberry Pi Micro Compute Cluster #

![20 Raspberry Pi Computer Cluster](https://bitbucket.org/repo/Bzr9oj/images/1485713203-DSC_0534%20-%2030%20percente.jpg)

## What is this repository for? ##

The Faculty of Engineering Science at the University of Greenwich hosted a Raspberry Jam event on 9th December 2015 where I presented my 20 nodes Raspberry Pi 2 Micro Compute Cluster. In advance of the event, a fair amount of work went in to setting up the cluster and this repository is being used to share this with the world.

This consists of a:

* 20 Pi stack
* 24 port switch
* Powered by a single ATX power supply
* NFS to boot all of the Pi's from a central server
* UnionFS to provide each Pi with a re-writeable rootfs
* QEMU server side for rootfs updates
* A number of scripts to support the development and management of the system.


## How do I get set up? ##

This repo provides a set of scripts to support the configuration and maintainance of NFS bootable Raspberry Pi images.

The idea is that one or more Raspberry Pi images can be constructed and hosted on an NFS server and one or more Raspberry Pi's can be used to boot their root filesystem from the NFS share.

An assumpting is made that you are using a Ubuntu Server / Client configuration although this may work with other Debian based Linux Distributions as well.

There are three parts to the NFS boot system.

### Server ###

The Server is the part of the system which is used to provide the NFS service to the Raspberry Pi and the Client. The server doesn't have to be a physical server or even run a server version of an operating system but it will offer the function of a several by running an NFS service.

To configure the server the NFS Kernel Server needs installing and configuring and the firewall needs opening to permit remote access.

The aim here is to end of with two exported directory shares on the server:

```
#!shell

/exports            # A read-write share used for creating and configuring images
/exports/piboot     # A read-only share used for a Raspberry Pi to boot from
```

This repo contains a script which automates the process of setting up a Ubuntu server with the necessary configuration to support the Client and Raspberry Pi sections further down.

The process is very straight forward, you just upload setup/configure_nfs_server_for_pi_boot.sh to your server and run it:

```
#!shell

sudo ./setup.sh
```

There is a big warning at the start because the resulting configuration is inherantly insecure. This is by design as a Raspberry Pi will not be using any form of authentication to connect to the NFS server to access the root filesystem image. You need to be aware of the risks and manage them carefully.


### Client ###

The Client is the part of the system which can be used to create an NFS hosted Raspberry Pi image, configure an image or construct an SD card which can boot the root filesystem from the server. In theory you could use the same system for server and client, providing that you system is network accessible by the Raspberry Pi's and your system as an SD card reader/writer.

There are 3 scripts in this repo to help with the process of creating, configuring images and deploying SD cards.


#### Image Creation ####

The image creation process consists of constructing an NFS ready Raspberry Pi operating system image on the server.

There are several steps to this process:
1. taking either a pre-configured SD card or an image file
2. extracting the partition table for use later
3. extracting the boot partition into an it's own image file for use on SD cards later
4. extracting the root filesystem into it's own directory on the NFS share
5. configuring the root filesystem with an additional step to the boot process to permit a union-fs root filesystem

To automate this process the client/create_nfs_bootable_image.sh script can be used.

The following examples make a few assumptions.

* The image name in the example is "pi-image". This name is used to differentiate the images on the NFS server as well as provide a unique hostname per client (comes later).
* The NFS server IP address in the example is "192.168.0.1". I would specify an IP inplace of a DNS name.
* The SD card device in the example is "sdd". If you want to find your device identifier, after inserting the SD card, run "dmesg | tail".

```
#!shell

# To create an image from an SD card:
sudo ./create_nfs_bootable_image.sh -c SD -d /dev/sdd -i pi-image -n 192.168.0.1

# To create an image from a local image file:
sudo ./create_nfs_bootable_image.sh -c IMG -s /tmp/source-pi.img -i pi-image -n 192.168.0.1

# To create an image from an online image file:
sudo ./create_nfs_bootable_image.sh -c URL -u https://downloads.raspberrypi.org/raspbian_latest -i pi-image -n 192.168.0.1
```

#### Image Modification ####

After an image on the server has been created, there will likely be a need to make modifications to the root filesystem, such as installing new packages. Thanks to QEMU's User Emulation, even though you are likely to be working on an Intel based processor, you can still run ARM based executables.

A script is provided, client/modify_nfs_bootable_image.sh, which automates the process of gaining access to the NFS hosted root filesystem to either run some commands or get to a command line.

```
#!shell

# To access a Pi root filesystem command line:
sudo ./modify_nfs_bootable_image.sh -i pi-image -n 192.168.0.1

# To execute a single command in the root filesystem:
sudo ./modify_nfs_bootable_image.sh -i pi-image -n 192.168.0.1 -c "apt-get update"

# To execute a set of commands in the root filesystem:
sudo ./modify_nfs_bootable_image.sh -i pi-image -n 192.168.0.1 -c "apt-get update; apt-get install curl"
```

#### SD Card Deployment ####

Once an image is created on the NFS server, you can use the client/deploy_nfs_bootable_sdcard.sh script to place an NFS bootable image onto the SD card. This process will wipe an SD card, recreating any partition structure that may already exists. It populates the boot partition with the contents from the original boot image and updates it to boot from the NFS share.

The process for using the script is relatively straight forward. Again, assuming the device identifier from your SD card is "sdd", you NFS hosted image name is "pi-image" and your NFS server IP is "192.168.0.1" and the node identifer is "1". The image name is used wit the node identifier to provide the Raspberry Pi with a unique hostname, eg pi-image-1, pi-image-2, and so on and a unique IP address.

Before you do, please note, it is best to use the servers IP address here as it will be used in setting up the NFS address for the Pi to boot from.


```
#!shell

# To leave the default IP address, netmask, dns server and gateway, you can run:
sudo client/deploy_nfs_bootable_sdcard.sh -d /dev/sdd -i pi-image -n 192.168.0.1 -u 1

# To set the network details...
#     -a - IP Address Prefix, defaults to 192.168.137.1
#     -m - Netmask, defaults to 255.255.255.0
#     -l - DNS server, defaults to 8.8.8.8
#     -g - Gateway, defaults to 192.168.137.1
sudo client/deploy_nfs_bootable_sdcard.sh -d /dev/sdd -i pi-image -n 192.168.0.1 -u 1 -a 192.168.137.1 -m 255.255.255.0 -l 8.8.8.8 -g 192.168.137.1
```


### Raspberry Pi's ###

There are several scripts included in the cluster/ folder of the repo that offer some services to support the management of a cluster and execution of commands across the cluster.

To include these in your NFS hosted root filesystem you need to fix them to work within a specific IP range. This is a constraint at this point enforcing the root filesystem to have the client IP range included which so far it is IP agnostic.

To include the cluster scripts in the image creation process, you can do this:

```
#!shell

# To create an image from a local image file with the cluster tools configured to work with IP's in the range of 192.168.137.101 to 192.168.137.199:
sudo ./create_nfs_bootable_image.sh -c IMG -s /tmp/source-pi.img -i pi-image -n 192.168.0.1 -C 192.168.137.1
```

The cluster tools provide the ability to find other nodes within the given IP range but searching through pinging the SSH port of the range of IPs and either returning available IPs or those not available.

For example, assuming you have a cluster of 30 nodes and you want a list of those which are "alive":

```
#!shell

cluster-find 30
```

If you want a list of nodes which are not "alive":

```
#!shell

cluster-missing 30
```

To run a command on all nodes on the cluster, by issuing the command by via SSH, for example to install the apt curl package, you can use:

```
#!shell

cluster-run 30 "apt-get update; apt-get install curl"
```

A couple of predefined scripts for ease are

```
#!shell

# To power off all nodes in the cluster
cluster-poweroff 30

# To reboot all nodes in the cluster
cluster-reboot 30
```

## Who do I talk to? ##

Dr Jodie Wetherall <wj88@gre.ac.uk>

Faculty of Engineering Science, University of Greenwich