#! /bin/bash

# Created by 2013 Jodie Wetherall <wj88@gre.ac.uk>
# Licensed under terms of GPLv3

SCRIPTPATH=$0
PACKAGES="nfs-common nfs-kernel-server ufw"

usage()
{
	echo -e "\nUsage: sudo $0"
	exit 1
}

checkpackages()
{
        # Check through our package list to see if any
        # need installing.
        NEEDSINSTALL=0
        for PACKAGE in $PACKAGES; do
                dpkg -s $PACKAGE >/dev/null 2>/dev/null
                if [ $? -ne 0 ]; then
                        NEEDSINSTALL=1
                fi
        done
        # If Any require installation, start the install process.
        if [ $NEEDSINSTALL -ne 0 ]; then
                echo -e "Installing prerequisite packages..." | tee -a $LOGFILE
                apt-get update >>$LOGFILE 2>>$LOGFILE
                apt-get -y install $PACKAGES >>$LOGFILE 2>>$LOGFILE
        fi
}

# We must be root to run!
if [[ $EUID -ne 0 ]]; then
        echo -e "You must run this script as root."
        usage
fi

echo -e "\nRaspbery Pi NFS Boot Server Configuration Script\n================================================\n\nWritten by Dr Jodie Wetherall <wj88@gre.ac.uk>\n"

echo -e "\nThis script will attempt to automatically install and configure your system"
echo -e "to run the NFS service and provide access to the client oriented scripts for"
echo -e "creating and modifying NFS hosted Raspberry Pi images as well as booting"
echo -e "Raspberry Pi's directly from the NFS share."

echo -e "\nThe things that are wrong with this configuration include:"
echo -e "* We're allowing access to all clients *() - this should ideally been limited"
echo -e "  to a specified set of clients."
echo -e "* We're using no_root_squash! As we placing a root filesystem on the server"
echo -e "  and need to access this as root from a client booting off the NFS mounted"
echo -e "  filesystem, we need root access. This is not secure!"
echo -e "* We're using insecure to permit access to IP ports greater than 1024."
echo -e "* The firewall ports are opened allowing access from anywhere to your server"
echo -e "  to connect to the NFS. The ports opened are 111, 2049 and 13100"
echo -e "\nYou can modify the server configured after this script has run to lock things"
echo -e "down a tighter if you wish or access the risks outlined.\n"

read -p "Are you sure you wish to continue? [y/n] " CONSENT
if [ "$CONSENT" = "Y" ]; then
        CONSENT=y
else
        if [ "$CONSENT" != "y" ]; then
		echo -e "\nFair enough. Think about it, you can always run this command later.\n"
                exit 1
        fi
fi

echo -e "\nConfiguration NFS..."

checkpackages

if [ ! -d /exports ]; then
	echo -e " * Creating /exports..."
	mkdir -p /exports
	chmod 777 /exports
else
	echo -e " * Directory /exports already exists."
fi

if [ ! -d /exports/piboot ]; then
	echo -e " * Creating /exports/piboot..."
	mkdir -p /exports/piboot
	chmod 777 /exports/piboot
else
	echo -e " * Directory /exports/piboot already exists."
fi

# We're using sync as per the spec, async apparently violates the NFS spec.
# We're using no_subtree_check, although apparently this is the default now.


# Does the exports file contain an export for the /exports path?
if [ $(cat /etc/exports | grep -v '^$\|^\s*\#' | grep -v '^/exports/piboot' | grep '^/exports' | wc -l) -eq 0 ]; then
	echo -e " * Configuring the NFS export for the /exports path..."
	echo -e "/exports\t*(rw,insecure,sync,no_root_squash,no_subtree_check)" >> /etc/exports
else
	echo -e " * Export for /exports already configured."
fi

# Does the exports file contain an export for the /exports/piboot path?
if [ $(cat /etc/exports | grep -v '^$\|^\s*\#' | grep '^/exports/piboot' | wc -l) -eq 0 ]; then
	echo -e " * Configuring the NFS export for the /exports/piboot path..."
	echo -e "/exports/piboot\t*(ro,insecure,sync,no_root_squash,no_subtree_check)" >> /etc/exports
else
	echo -e " * Export for /exports/piboot already configured."
fi

if [ $(cat /etc/default/nfs-kernel-server | grep "RPCMOUNTDOPTS=\"-p 13100\"" | wc -l) -eq 0 ]; then
	echo -e " * Configuring the nfs server to use RPC port 13100..."
	# Remove the # from the start of the RPCMOUNTDOPTS line and change its value.
	cat /etc/default/nfs-kernel-server | sed -e 's/^#RPCMOUNTDOPTS=/RPCMOUNTDOPTS=/' -e '/^RPCMOUNTDOPTS=/s/=.*/=\"-p 13100\"/' >/etc/default/nfs-kernel-server
else
	echo -e " * RPC port for NFS already configured."
fi

echo -e "\nConfiguring the firewall..."
ufw allow from any to any port 111 | sed 's/^/ * /'
ufw allow from any to any port 2049 | sed 's/^/ * /'
ufw allow from any to any port 13100 | sed 's/^/ * /'

echo -e "\nRestarting the nfs server..."
service nfs-kernel-server restart

echo -e "\nAll done.\n\nYou now have 2 exported NFS shares:\n\t/exports\tUsed for ready/write access\n\t/exports/piboot\tUsed for read-only access\n\nPlease note that you are responsible for the security of your server!\n"
